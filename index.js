import { createFilter } from 'rollup-pluginutils';
import { processString } from 'uglifycss';
import { html } from 'lit-html';

/**
 * Imports css as shady-css compatible 'style modules' aka inline elements
 * @param  {Object} [options={}]
 * @return {Object}
 */
export default function shadycss({ include = ['**/*.css'], exclude, uglify } = {}) {
  const filter = createFilter(include, exclude);
  return {
    name: 'shadyCss',

    transform(css, id) {
      if (id.slice(-4) !== '.css') return null;
      if (!filter(id)) return null;
      const output = html`<style>${uglify ? processString(css) : css}</style>`;
      const code = `export default ${output};`;
      const map = { mappings: '' };
      return { code, map };
    },
  };
}
