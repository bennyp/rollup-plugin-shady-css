# rollup-plugin-shady-css

Import css files as lit-html templates

```js
import shadycss from 'rollup-plugin-shady-css';
export default {
  ...config,
  plugins: [
    shadycss({
      include: ['**/*.css'],
      exclude: undefined,
      uglify: undefined,
    })
  ]
}
